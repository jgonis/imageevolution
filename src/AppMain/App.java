package AppMain;

import Controllers.ImageLoadController;
import Controllers.StartStopController;
import Models.Model;
import Views.EvolutionWindow;
import org.opencv.core.Core;

import javax.swing.*;

public class App {

	public static void main(String[] args) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Model model = new Model();
				ImageLoadController imageLoadController = new ImageLoadController(model);
				StartStopController startStopController = new StartStopController(model);
				EvolutionWindow ew = new EvolutionWindow(imageLoadController, startStopController, model);
				ew.show();
			}
		});

	}
}
