package Controllers;

import Models.Interfaces.ImageModel;
import Utils.ImageFileFilter;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class ImageLoadController {

	public ImageLoadController(ImageModel model){
		super();
		m_model = model;
	}

	public void loadClicked(ActionEvent e) {
		JFileChooser jfc = new JFileChooser();
		jfc.setFileFilter(new ImageFileFilter());
		jfc.setMultiSelectionEnabled(false);
		jfc.setAcceptAllFileFilterUsed(false);
		int retVal = jfc.showOpenDialog(null);
		if(retVal == JFileChooser.APPROVE_OPTION) {
			File selectedFile = jfc.getSelectedFile();
			System.out.println(selectedFile.getPath());
			try {
				BufferedImage bi = ImageIO.read(selectedFile);
				m_model.loadNewImage(bi);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}

		}
	}

	private ImageModel m_model;
}
