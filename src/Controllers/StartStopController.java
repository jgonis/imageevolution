package Controllers;

import Models.Interfaces.EvolutionProcessModel;
import Models.Model;

import java.awt.event.ActionEvent;

public class StartStopController {

	public StartStopController(EvolutionProcessModel model){
		super();
		m_model = model;
	}

	public void startEvolution(ActionEvent e) {
		m_model.startApp();
	}

	public void stopEvolution(ActionEvent e) {
		m_model.stopApp();
	}

	private EvolutionProcessModel m_model;

}
