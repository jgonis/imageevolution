package Views;

import Controllers.ImageLoadController;
import Controllers.StartStopController;
import Models.Model;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;

public class EvolutionWindow {

	public EvolutionWindow(ImageLoadController imageLoadController,
	                       StartStopController startStopController,
	                       Model model) {
		m_mainFrame.setMinimumSize(new Dimension(1024, 768));
		m_mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		m_imageLoadController = imageLoadController;
		m_startStopController = startStopController;
		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new BorderLayout());
		m_mainFrame.setContentPane(contentPanel);
		m_drawingPanel = new DrawingPanel(model);
		m_ctrlPanel = new ControlPanel(m_imageLoadController, m_startStopController, model);
		contentPanel.add(m_drawingPanel, BorderLayout.CENTER);
		contentPanel.add(m_ctrlPanel.getPanel(), BorderLayout.SOUTH);
	}

	public void show() {
		m_mainFrame.setVisible(true);
	}
	private ImageLoadController m_imageLoadController;
	private StartStopController m_startStopController;
	private JFrame m_mainFrame = new JFrame("Image Evolution");
	private DrawingPanel m_drawingPanel;
	private ControlPanel m_ctrlPanel;

}
