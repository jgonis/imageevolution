package Views;

import Controllers.ImageLoadController;
import Controllers.StartStopController;
import Models.Interfaces.EvolutionProcessModel;
import Views.Interfaces.Notifiable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class ControlPanel implements Notifiable {
	public ControlPanel(ImageLoadController imageLoadController, StartStopController startStopController,
	                    EvolutionProcessModel model) {
		m_imageLoadController = imageLoadController;
		m_startStopController = startStopController;
		m_controlPanel.add(m_loadButton, -1);
		m_controlPanel.add(m_startButton, -1);
		m_controlPanel.add(m_stopButton, -1);
		m_controlPanel.add(m_iterationSpeed);
		m_model = model;
		m_model.subscribeToAppStateUpdates(this);

		m_loadButton.addActionListener((ActionEvent e) -> {
			m_imageLoadController.loadClicked(e);
		});

		m_startButton.addActionListener((ActionEvent e) -> {
			m_startStopController.startEvolution(e);
		});

		m_stopButton.addActionListener((ActionEvent e) -> {
			m_startStopController.stopEvolution(e);
		});
		this.update();
	}

	@Override
	public void update() {
		if(m_model.isAppStopped()) {
			if(m_model.hasImage() == true)
				m_startButton.setEnabled(true);
			else
				m_startButton.setEnabled(false);
			m_stopButton.setEnabled(false);
			m_loadButton.setEnabled(true);
		} else {
			m_startButton.setEnabled(false);
			m_stopButton.setEnabled(true);
			m_loadButton.setEnabled(false);
		}
		m_iterationSpeed.setText(m_model.getIterationSpeed());
	}

	public Component getPanel() {
		return m_controlPanel;
	}

	private JPanel m_controlPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
	private ImageLoadController m_imageLoadController;
	private StartStopController m_startStopController;
	private JButton m_loadButton = new JButton("Load");
	private JButton m_startButton = new JButton("Start");
	private JButton m_stopButton = new JButton("Stop");
	private JLabel m_iterationSpeed = new JLabel("");
	private EvolutionProcessModel m_model;
}
