package Views;

import Models.Interfaces.ImageModel;
import Utils.AspectRatioCalculations;
import Views.Interfaces.Notifiable;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class DrawingPanel extends JPanel implements Notifiable {

	public DrawingPanel(ImageModel model) {
		super.setBackground(Color.WHITE);
		m_model = model;
		m_model.subscribeToImageUpdates(this);
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (m_image != null) {
			int[] imageDrawingBounds = AspectRatioCalculations.calcs(this.getBounds(), m_image.getWidth(), m_image.getHeight());
			Graphics2D g2d = (Graphics2D) g;
			g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
			g2d.drawImage(m_image,
					imageDrawingBounds[0],
					imageDrawingBounds[1],
					imageDrawingBounds[2],
					imageDrawingBounds[3],
					null);
		}
	}

	@Override
	public void update() {
		m_image = m_model.getCurrentImage();
		this.repaint();
	}

	private BufferedImage m_image;
	private ImageModel m_model;
}
