package Views.Interfaces;

public interface Notifiable {
	void update();
}
