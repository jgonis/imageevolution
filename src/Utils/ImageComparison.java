package Utils;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;

public final class ImageComparison {

	public ImageComparison(BufferedImage originalImage) {
		BufferedImage image = ImageCreation.convertImageToByteType(originalImage);
		m_originalMat = ImageComparison.getMatrixFromImage(image);
		m_destinationMat = Mat.zeros(m_originalMat.rows(), m_originalMat.cols(), CvType.CV_8UC3);
	}

	public final double diffImageAgainstOriginal(Mat newImage) {
		Core.absdiff(newImage, m_originalMat, m_destinationMat);
		Scalar result = Core.sumElems(m_destinationMat);
		double[] results = result.val;
		double sum = 0;
		for(int i = 0; i < results.length; i++) {
			sum += results[i];
		}
		return sum;
	}

	public static Mat getMatrixFromImage(BufferedImage bi) {
		BufferedImage appropriateImage = ImageCreation.convertImageToByteType(bi);
		DataBuffer pixels = appropriateImage.getRaster().getDataBuffer();
		DataBufferByte pixBuff = (DataBufferByte) pixels;
		Mat mat = new Mat(appropriateImage.getHeight(), appropriateImage.getWidth(), CvType.CV_8UC3);
		mat.put(0, 0, pixBuff.getData());
		return mat;
	}

	public static void getMatrixFromImage(BufferedImage bi, Mat destMat) {
		BufferedImage appropriateImage = ImageCreation.convertImageToByteType(bi);
		DataBuffer pixels = appropriateImage.getRaster().getDataBuffer();
		DataBufferByte pixBuff = (DataBufferByte) pixels;
		destMat.put(0, 0, pixBuff.getData());
	}

	private Mat m_originalMat;
	private Mat m_destinationMat;
}
