package Utils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.VolatileImage;

public final class ImageCreation {

	public static VolatileImage createImage(int imageWidth, int imageHeight) {
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gd = ge.getDefaultScreenDevice();
		GraphicsConfiguration gc = gd.getDefaultConfiguration();
		VolatileImage vi = gc.createCompatibleVolatileImage(imageWidth, imageHeight);

		Graphics g = vi.getGraphics();
		g.setColor(Color.white);
		g.fillRect(0, 0, vi.getWidth(), vi.getHeight());
		g.dispose();
		System.out.println(vi.getSnapshot().getType());
		return vi;
	}

	public static BufferedImage convertImageToByteType(BufferedImage bi) {
		if(bi.getType() == BufferedImage.TYPE_3BYTE_BGR) {
			return bi;
		}

		BufferedImage image = new BufferedImage(bi.getWidth(), bi.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		Graphics g = image.getGraphics();
		g.drawImage(bi, 0, 0, null);
		g.dispose();

		return image;
	}
}
