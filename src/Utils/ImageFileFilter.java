package Utils;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class ImageFileFilter extends FileFilter {
	@Override
	public boolean accept(File f) {
		if (f == null)
			return false;

		if (f.isDirectory()) {
			return true;
		}

		String extension = this.getExtension(f);
		if (extension == null)
			return false;

		if (extension.equals("jpg") ||
				extension.equals("jpeg") ||
				extension.equals("png")) {
			return true;
		} else {
			return false;
		}
	}

	private String getExtension(File f) {
		String ext = null;
		String s = f.getName();
		int i = s.lastIndexOf('.');

		if (i > 0 && i < s.length() - 1) {
			ext = s.substring(i + 1).toLowerCase();
		}
		return ext;
	}

	@Override
	public String getDescription() {
		return "Just .png or .jpg";
	}
}
