package Utils;

import java.awt.*;

public class AspectRatioCalculations {

	public static int[] calcs(Rectangle canvasBounds, int imageWidth, int imageHeight) {
		double widthOverHeight = imageWidth / (imageHeight * 1.0);
		double heightOverWidth = imageHeight / (imageWidth * 1.0);
		double possibleWidth = Math.floor(canvasBounds.getHeight() * widthOverHeight);
		double possibleHeight = Math.floor(canvasBounds.getWidth() * heightOverWidth);
		if (possibleWidth < canvasBounds.getWidth()) {
			double calculatedHeight = Math.floor(possibleWidth * heightOverWidth);
			if (calculatedHeight > canvasBounds.getHeight())
				assert (false);
			return centerImage(canvasBounds, Math.toIntExact(Math.round(possibleWidth)), Math.toIntExact(Math.round(calculatedHeight)));

		} else if (possibleHeight < canvasBounds.getHeight()) {
			double calculatedWidth = Math.floor(possibleHeight * widthOverHeight);
			if (calculatedWidth > canvasBounds.getWidth())
				assert (false);
			return centerImage(canvasBounds, Math.toIntExact(Math.round(calculatedWidth)), Math.toIntExact(Math.round(possibleHeight)));

		} else {
			assert (false);
			return new int[]{0, 0, 0, 0};
		}
	}

	static int[] centerImage(Rectangle canvasBounds, int imageWidth, int imageHeight) {
		double centerX = canvasBounds.getCenterX();
		double centerY = canvasBounds.getCenterY();
		int left = Math.toIntExact(Math.round(centerX - (imageWidth / 2)));
		int top = Math.toIntExact(Math.round(centerY - (imageHeight / 2)));
		assert (left >= 0);
		assert (top >= 0);
		return new int[]{left, top, imageWidth, imageHeight};
	}
}
