package EvolutionThread;

import Models.Interfaces.ImageModel;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

public class EvolutionThreadCoordinator implements Runnable {

	public EvolutionThreadCoordinator(ImageModel model, BufferedImage originalImage) {
		m_model = model;
		m_originalImage = originalImage;
	}


	@Override
	public void run() {
		int numberOfThreadsToSpawn = Runtime.getRuntime().availableProcessors();
		for(int i = 0; i < numberOfThreadsToSpawn; i++) {
			m_threads.put(new Thread(new EvolutionThread(m_model, m_originalImage, m_resultQueue)), new LinkedBlockingQueue<>());
		}
	}

	private Map<Thread, LinkedBlockingQueue<BufferedImage>> m_threads = new HashMap<>();
	private ImageModel m_model;
	private BufferedImage m_originalImage;
	private LinkedBlockingQueue<BufferedImage> m_resultQueue;

}
