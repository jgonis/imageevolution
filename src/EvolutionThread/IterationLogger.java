package EvolutionThread;

import Models.Interfaces.ImageModel;

import javax.swing.*;

public final class IterationLogger {
	public IterationLogger(ImageModel model) {
		m_model = model;
	}

	public final void checkIterationCount() {
		if (m_iterationCount > 100) {
			m_iterationCount = 0;
			logIterationTime(m_startTime);
			m_startTime = System.nanoTime();
		}
	}

	public final void iterate() {
		m_iterationCount++;
	}

	private void logIterationTime(long startTime) {
		long duration = System.nanoTime() - startTime;
		double time = duration / 1000000.0;
		SwingUtilities.invokeLater(new UIThreadIterationsUpdater(time, m_model));
	}

	private int m_iterationCount = 0;
	private long m_startTime = System.nanoTime();
	private ImageModel m_model;
}
