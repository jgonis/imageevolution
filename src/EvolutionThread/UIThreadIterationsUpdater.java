package EvolutionThread;

import Models.Interfaces.ImageModel;

public class UIThreadIterationsUpdater implements Runnable {
	public UIThreadIterationsUpdater(double time, ImageModel model) {
		m_model = model;
		m_time = time;

	}

	@Override
	public void run() {
		m_model.timeFor100Iterations(m_time);
	}

	private double m_time;
	private ImageModel m_model;
}
