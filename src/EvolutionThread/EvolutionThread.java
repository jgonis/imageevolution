package EvolutionThread;

import Models.Interfaces.ImageModel;
import Utils.ImageComparison;
import org.opencv.core.Mat;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

public class EvolutionThread implements Runnable {

	public EvolutionThread(ImageModel model, BufferedImage originalImage, BlockingQueue<BufferedImage> resultQueue, BlockingQueue<BufferedImage> updateQueue) {
		m_imageComparisonObj = new ImageComparison(originalImage);
		m_scratchImage = new BufferedImage(originalImage.getWidth(), originalImage.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		m_currentImage = new BufferedImage(originalImage.getWidth(), originalImage.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		m_model = model;
		m_updateQueue = updateQueue;
		m_resultQueue = resultQueue;
	}

	@Override
	public void run() {
		this.decideOnStartingColor(m_scratchImage);
		Graphics2D scratchGraphics = (Graphics2D) m_scratchImage.getGraphics();
		Graphics2D currentGraphics = (Graphics2D) m_currentImage.getGraphics();
		currentGraphics.drawImage(m_scratchImage, 0, 0, null);
		int[] xPoints = {0,0,0};
		int[] yPoints = {0,0,0};
		Mat scratchImageMat = ImageComparison.getMatrixFromImage(m_scratchImage);
		double currentDifference = m_imageComparisonObj.diffImageAgainstOriginal(scratchImageMat);
		IterationLogger logger = new IterationLogger(m_model);
		while (true) {
			//check if interrupted and if so stop
			if (Thread.interrupted())
				break;
			if(m_updateQueue.peek() != null) {
				try {
					BufferedImage updateImage = m_updateQueue.take();
					currentGraphics.drawImage(updateImage, 0, 0, null);
				} catch (InterruptedException e) {
					break;
				}
				
			}
			logger.checkIterationCount();
			drawRandomTriangle(scratchGraphics, xPoints, yPoints);
			ImageComparison.getMatrixFromImage(m_scratchImage, scratchImageMat);

			double newDifference = m_imageComparisonObj.diffImageAgainstOriginal(scratchImageMat);

			if(newDifference < currentDifference) {
				currentGraphics.drawImage(m_scratchImage, 0, 0, null);
				currentDifference = newDifference;
				BufferedImage resultImage = new BufferedImage(m_currentImage.getWidth(), m_currentImage.getHeight(), m_currentImage.getType());
				Graphics graphics = resultImage.getGraphics();
				graphics.drawImage(m_currentImage, 0, 0, null);
				graphics.dispose();
				m_resultQueue.offer(resultImage);
			} else {
				scratchGraphics.drawImage(m_currentImage, 0, 0, null);
			}
			logger.iterate();
		}
		scratchGraphics.dispose();
		currentGraphics.dispose();
	}

	private void logIterationTime(long startTime) {
		long duration = System.nanoTime() - startTime;
		double time = duration / 1000000.0;
		SwingUtilities.invokeLater(new UIThreadIterationsUpdater(time, m_model));
	}

	private void drawRandomTriangle(Graphics2D scratchGraphics, int[] xPoints, int[] yPoints) {
		//To generate new candidate take current candidate image
		xPoints[0] = m_random.nextInt(m_currentImage.getWidth());
		xPoints[1] = m_random.nextInt(m_currentImage.getWidth());
		xPoints[2] = m_random.nextInt(m_currentImage.getWidth());
		yPoints[0] = m_random.nextInt(m_currentImage.getHeight());
		yPoints[1] = m_random.nextInt(m_currentImage.getHeight());
		yPoints[2] = m_random.nextInt(m_currentImage.getHeight());
		scratchGraphics.setColor(new Color(m_random.nextInt(256),
				m_random.nextInt(256),
				m_random.nextInt(256),
				m_random.nextInt(256)));
		scratchGraphics.fillPolygon(xPoints, yPoints, xPoints.length);
	}

	private void decideOnStartingColor(BufferedImage img) {
		Graphics2D g2d = (Graphics2D) img.getGraphics();
		g2d.setColor(Color.white);
		g2d.fillRect(0, 0, img.getWidth(), img.getHeight());
		Mat currentMat = ImageComparison.getMatrixFromImage(img);
		double whiteResult = m_imageComparisonObj.diffImageAgainstOriginal(currentMat);
		g2d.setColor(Color.black);
		g2d.fillRect(0, 0, img.getWidth(), img.getHeight());
		ImageComparison.getMatrixFromImage(img, currentMat);
		double blackResult = m_imageComparisonObj.diffImageAgainstOriginal(currentMat);
		if (whiteResult < blackResult) {
			g2d.setColor(Color.white);
			g2d.fillRect(0, 0, img.getWidth(), img.getHeight());
		}
		g2d.dispose();
	}

	private ImageComparison m_imageComparisonObj;
	private ImageModel m_model;
	private BufferedImage m_scratchImage;
	private BufferedImage m_currentImage;
	BlockingQueue<BufferedImage> m_resultQueue;
	BlockingQueue<BufferedImage> m_updateQueue;
	private ThreadLocalRandom m_random = ThreadLocalRandom.current();
}
