package Models;

import EvolutionThread.EvolutionThread;
import Models.Interfaces.EvolutionProcessModel;
import Models.Interfaces.ImageModel;
import Views.Interfaces.Notifiable;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Model implements ImageModel, EvolutionProcessModel {

	public Model() {
		m_isStopped = true;
		m_iterations = "0 per second";
	}

	@Override
	public void loadNewImage(BufferedImage img) {
		m_originalImage = img;
		for(Notifiable observer : m_imageObservers) {
			observer.update();
		}
		for(Notifiable observer : m_appStateObservers) {
			observer.update();
		}
	}

	@Override
	public void subscribeToImageUpdates(Notifiable observer) {
		if(m_imageObservers.contains(observer) == false) {
			m_imageObservers.add(observer);
		}
	}

	@Override
	public BufferedImage getCurrentImage() {
		return m_originalImage;
	}

	@Override
	public boolean isAppStopped() {
		return m_isStopped;
	}

	@Override
	public boolean hasImage() {
		if(m_originalImage != null)
			return true;
		else
			return false;
	}

	@Override
	public void startApp() {
		if(m_isStopped != false) {
			m_isStopped = false;
			if(m_evolutionThread != null) {
				m_evolutionThread.interrupt();
			}
			m_evolutionThread = new Thread(new EvolutionThread(this, m_originalImage));
			m_evolutionThread.start();
			for (Notifiable observer : m_appStateObservers) {
				observer.update();
			}
		}
	}

	@Override
	public void stopApp() {
		if(m_isStopped != true) {
			m_isStopped = true;
			if(m_evolutionThread != null) {
				m_evolutionThread.interrupt();
			}
			for (Notifiable observer : m_appStateObservers) {
				observer.update();
			}
		}
	}

	@Override
	public void subscribeToAppStateUpdates(Notifiable observer) {
		if(m_appStateObservers.contains(observer) == false) {
			m_appStateObservers.add(observer);
		}
	}

	public String getIterationSpeed() {
		return m_iterations;
	}

	public void timeFor100Iterations(double time) {
		double val = 100.0 / (time / 1000);
		m_iterations = Double.valueOf(Math.round(val)).toString() + " iterations per second";
		for(Notifiable observer : m_appStateObservers) {
			observer.update();
		}
	}

	private ArrayList<Notifiable> m_imageObservers = new ArrayList<>();
	private ArrayList<Notifiable> m_appStateObservers = new ArrayList<>();
	private BufferedImage m_originalImage;
	private boolean m_isStopped;
	private Thread m_evolutionThread;
	private String m_iterations;
}
