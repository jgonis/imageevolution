package Models.Interfaces;

import Views.Interfaces.Notifiable;

public interface EvolutionProcessModel {
	boolean isAppStopped();
	boolean hasImage();
	void startApp();
	void stopApp();
	String getIterationSpeed();
	void subscribeToAppStateUpdates(Notifiable observer);

}
