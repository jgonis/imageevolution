package Models.Interfaces;

import Views.Interfaces.Notifiable;

import java.awt.image.BufferedImage;

public interface ImageModel {
	void loadNewImage(BufferedImage img);
	void subscribeToImageUpdates(Notifiable observer);
	BufferedImage getCurrentImage();
	void timeFor100Iterations(double time);
}
